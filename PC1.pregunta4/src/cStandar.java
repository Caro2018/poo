public class cStandar {
    public Float a;
    public Float b;

    public cStandar(Float a, Float b) {
        this.a = a;
        this.b = b;
    }

    public Float sumar(){
        return a+b;
    }
    public Float restar(){
        return a-b;
    }
    public Float multiplicar(){
        return a*b;
    }
    public Float dividir(){
        return a/b;
    }
}
