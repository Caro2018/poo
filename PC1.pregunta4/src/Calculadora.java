public class Calculadora {

    public static void main(String[] args) {
        cStandar standar = new cStandar(2.5f,3.6f);
        System.out.println("Calculadora Standar");
        System.out.println("**********************");
        System.out.println("Suma: "+standar.sumar());
        System.out.println("Resta: "+standar.restar());
        System.out.println("Multiplicacion :"+standar.multiplicar());
        System.out.println("Division :"+standar.dividir());

        System.out.println();

        cCientifica cientifica = new cCientifica(70f, 30f);
        System.out.println("Calculadora Cientifica");
        System.out.println("***********************");
        System.out.println("Suma: "+cientifica.sumar());
        System.out.println("Resta: "+cientifica.restar());
        System.out.println("Mutiplicacion: "+cientifica.multiplicar());
        System.out.println("Divsion: "+cientifica.dividir());
        System.out.println("b% de a : "+cientifica.hallar_porcentaje());
    }
}
