public class Venta {
    private Producto producto;
    private Integer cantidad;
    private Float montoTotal;
    private Float igv;
    public void imprimir(){
        System.out.println("Producto: " +
                producto.getMarca()+" "+
                producto.getModelo()+" "+
                producto.getColor()
        );
        System.out.println("Precio unitario: "+producto.getPrecio());
        System.out.println("Cantidad: "+cantidad);
        System.out.println(": "+);
        System.out.println(": "+);
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public Float getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(Float montoTotal) {
        this.montoTotal = montoTotal;
    }

    public Float getIgv() {
        return igv;
    }

    public void setIgv(Float igv) {
        this.igv = igv;
    }
}
