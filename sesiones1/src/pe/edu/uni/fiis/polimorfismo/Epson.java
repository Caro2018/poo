package pe.edu.uni.fiis.polimorfismo;

public class Epson extends Impresora implements Eficiente,Renovable{
    public String imprimir() {
        return "epson";
    }
    // La herencia es unica...
    public Integer potenciar() {
        return 19;
    }

    public String reciclar() {
        return "renovación";
    }

    public String mensaje() {
        System.out.println("mensaje epson");
        return "mensaje Epson";
    }
    public String mensaje(String dato){
        System.out.println("mensaje"+dato);
        return "mensaje Epson"
    }
}
