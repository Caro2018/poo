class Carnet {
    String dni;
    String nombres;
    String fechaExpiracion;
    float ancho;
    double peso;
    String color;
    String codigo;

    public void imprimir(){
        System.out.println("El DNI es: "+dni);
        System.out.println("Nombres: "+nombres);
    }
    public static void main(String[] args) {
        Carnet carnet = new Carnet();
        carnet.dni = "77777777";
        carnet.nombres = "Juan Marcos";
        carnet.imprimir();
        Carnet carnet1 = carnet;
        carnet1.dni="232323232323";
        carnet.imprimir();
        carnet1.imprimir();
    }
}
