package pe.edu.uni.fiis.excel;

import com.sun.scenario.effect.impl.sw.java.JSWBlend_BLUEPeer;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class Excel {
    public static void main(String[] args) {
        Workbook workbook = new XSSFWorkbook();
        Sheet hoja = workbook.createSheet("Mi Hoja");

        /*
        for(int i=0;i<11;i++){

            for (int j=0;j<4;j++){
                Row fila = hoja.createRow(i);
                Cell celda = fila.createCell(j);
                celda.setCellValue(array[i][j]);
            }
        }
        */

        Row fila = hoja.createRow(0);
        Cell celda = fila.createCell(0);
        celda.setCellValue("Mi ejemplo");
        Cell celda1 = fila.createCell(1);
        celda1.setCellValue(Color.BLUE.getNombre());
        Cell celda2 = fila.createCell(2);
        celda2.setCellValue(Color.GREEN.getNombre());

        File archivo = new File( "D:\\poo1\\repositorio\\poo\\archivo.xlsx");
        try{
            FileOutputStream f = new FileOutputStream(archivo);
            workbook.write(f);
            workbook.close();
        }catch (FileNotFoundException e){
            e.printStackTrace();
        } catch (IOException e){
            e.printStackTrace();
        }
    }
}
